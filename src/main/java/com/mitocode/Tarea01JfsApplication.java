package com.mitocode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tarea01JfsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Tarea01JfsApplication.class, args);
	}
}
